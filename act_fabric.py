from osv import fields, osv
from tools.translate import _


class act_fabric(osv.osv):
	def _all_cost_pc(self, cr, uid, ids, field_name, arg, context=None):
		res={}
		for fab in self.browse(cr, uid, ids, context=context):
			val = 0.0
			for line in fab.parts:
				val += line.total_cost
			res[fab.id] = val
		return res
		
	def _all_total_cost_doz(self, cr, uid, ids, field_name, arg, context=None):
		res={}
		for fab in self.browse(cr, uid, ids, context=context):
			if fab.all_cost_pc==0.0:
				res[fab.id] = 0.0
			else:
				res[fab.id] = fab.all_cost_pc*12.0
		return res
	
	_name = 'act.fabric'
	_description = 'Collection of parts'
	_columns = {
		'buyer_name': fields.char('Buyer Name', size=128, required=True),		
		'style_ref': fields.char('Style/Ref', size=128, required=True),
		'size_range': fields.char('Size Range', size=128, required=True),
		'fabrication': fields.char('Fabrication', size=128, required=True),
		'fabric_composition': fields.char('Fabric Composition', size=128, required=True),
		'gsm': fields.float('GSM Value', required=True),
		'parts': fields.one2many('act.fabric.parts','fabric_part_id','Fabric Parts'),
		'total_cost': fields.one2many('act.fabric.parts','total_cost','Total Cost'),
		
		'all_cost_pc': fields.function(_all_cost_pc, method=True, string='All Total Cost'),
		'all_total_cost_doz': fields.function(_all_total_cost_doz, method=True, string='All Total Cost per doz'),
		'note': fields.text('Notes'),
#		'info_tab_name': fields.char('Info tab name', size=128, required=True),		
#		'name': fields.char('Info tab value', size=128, required=True),
#		'info_tab_type': fields.selection([('per_yarn_price', 'Per yarn price'), ('lycra_price', 'Lycra price')], 'Info tab type', required=True, help="Will help")
	}
	def button_dummy(self, cr, uid, ids, context=None):
		return True

act_fabric()

class act_fabric_info_tab(osv.osv):
	
	_name = 'act.fabric.infotab'
	_description = 'Collection of parts infotab'
	_columns = {		
		'name': fields.char('Info Tab Name', size=128, required=True),
		'info_tab_value': fields.char('Info Tab Value', size=128, required=True),
		'info_tab_date': fields.date('Date Added', required=True, select=True),
		'info_tab_type': fields.selection([('per_yarn_price_info', 'Per yarn price'), ('lycra_price_info', 'Lycra price'), ('knitting_cost_info', 'Knitting cost Info'), ('dying_cost_info', 'Dying cost Info'), ('extra_finishing_cost_info', 'Extra finishing cost Info')], 'Info tab type', required=True, help="Will help")
	}

act_fabric_info_tab()

class act_fabric_parts(osv.osv):
	
	def _f_consumption_pc(self, cr, uid, ids, field_name, arg, context=None):
		res = {}
		if context is None:
			context = {}
		for line in self.browse(cr, uid, ids, context=context):	
			if line.p_type == 'fabric':
				#consumption = (((line.body_length+line.sleeve_length)*line.body_width*line.gsm)+((line.body_length+line.sleeve_length)*line.body_width*line.gsm)*line.wastage/100.00)
				consumption = (((line.body_length+line.sleeve_length)*line.body_width)+((line.body_length+line.sleeve_length)*line.body_width)*line.wastage/100.00)
			else:
				consumption = line.required_qty_pc_gmts			
			
			res[line.id] = consumption
		return res
	
	
	def _f_cost_pc(self, cr, uid, ids, field_name, arg, context=None):
		res = {}
		if context is None:
			context = {}
		
		for line in self.browse(cr, uid, ids, context=context):	
			if line.p_type == 'fabric':
				price = (line.yarn_count* line.per_yarn_price) + line.lycra_price + line.knitting_cost + line.dying_cost + line.extra_finishing_cost
			else:
				price = line.unit_price_pcs
							
			res[line.id] = price
		return res
	
	
	def _f_total_cost(self, cr, uid, ids, field_name, arg, context=None):
		res = {}
		if context is None:
			context = {}
		for line in self.browse(cr, uid, ids, context=context):			
			total_cost = line.consumption_pc * line.cost_pc
			
			res[line.id] = total_cost
		return res
	
	
	def _f_total_cost_doz(self, cr, uid, ids, field_name, arg, context=None):
		res = {}
		if context is None:
			context = {}
		for line in self.browse(cr, uid, ids, context=context):			
			total_cost_dozn = line.total_cost * 12
			
			res[line.id] = total_cost_dozn
		return res
	

	
	_name = 'act.fabric.parts'
	_description = 'Parts of a garment item'
	_columns = {
		'fabric_part_id': fields.many2one('act.fabric', 'Part Reference',required=True, ondelete='cascade', select=True),
		'name': fields.char('Part Name', size=128, required=True),
		'p_type': fields.selection([('fabric', 'Main Fabric'), ('other', 'Other parts')], 'Product Type', required=True, help="Will help"),
		
		'required_qty_pc_gmts': fields.float('Required Qty / pc/ gmts'),
		'unit_price_pcs': fields.float('Unit Price / Pcs'),
		
		'body_length': fields.float('Body length'),
		'body_width': fields.float('Body width'),
		'sleeve_length': fields.float('Sleeve length'),
		'wastage': fields.float('Wastage'),
		
		'yarn_count': fields.float('Yarn count'),
		
		'per_yarn_price': fields.float('Per yarn price'),		
		'per_yarn_price_info': fields.many2one('act.fabric.infotab','Yarn Price Info'),
		
		'lycra_price': fields.float('Lycra price'),
		'lycra_price_info': fields.many2one('act.fabric.infotab','Lycra Price Info'),
		
		'knitting_cost': fields.float('Knitting cost'),
		'knitting_cost_info': fields.many2one('act.fabric.infotab','Knitting cost Info'),
		
		'dying_cost': fields.float('Dying cost'),
		'dying_cost_info': fields.many2one('act.fabric.infotab','Dying cost Info'),
		
		'extra_finishing_cost': fields.float('Extra finishing cost'),
		'extra_finishing_cost_info': fields.many2one('act.fabric.infotab','Extra finishing cost Info'),
		
#		'product_manufacturer' : fields.many2one('res.partner', 'Manufacturer'),
		
		'consumption_pc': fields.function(_f_consumption_pc,method=True,string='Consumption'),
		'cost_pc': fields.function(_f_cost_pc,method=True,string='Cost/pc'),
		'total_cost': fields.function(_f_total_cost,method=True,string='Total cost/Doz'),
		'total_cost_doz': fields.function(_f_total_cost_doz,method=True,string='Total cost/Doz')
	}

act_fabric_parts()
