{
	"name" : "Activation Fabric",
    "version" : "1.0",
    "depends" : ["base"],
	"author" : "Activation Limited",
    "category": 'Generic Modules',
	"depends" : ["base"],
	"description" : "Simple demo module",
	"init_xml" : ["act_fabric_view.xml"],
	"demo_xml" : [],
	"update_xml" : [],
	"active": False,
	"installable": True
}

